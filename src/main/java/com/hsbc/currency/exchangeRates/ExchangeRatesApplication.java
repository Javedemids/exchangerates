package com.hsbc.currency.exchangeRates;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * <h1>ExchangeRatesApplication</h1>
 * 
 * @author Md Javed
 * @version 1.0
 * @since 2020-10-13
 */
@SpringBootApplication
public class ExchangeRatesApplication extends SpringBootServletInitializer {

	/**
	 * This is the main method which will start the application.
	 * 
	 * @param args
	 *            Unused.
	 * @return Nothing.
	 */
	public static void main(String[] args) {
		SpringApplication.run(ExchangeRatesApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ExchangeRatesApplication.class);
	}
}
