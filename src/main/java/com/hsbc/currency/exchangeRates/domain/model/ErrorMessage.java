package com.hsbc.currency.exchangeRates.domain.model;

/**
 * <h1>ErrorMessage</h1>
 * 
 * @author Md Javed
 * @version 1.0
 * @since 2020-10-13
 */
public class ErrorMessage {
	private String errorMessage;
	private int errorCode;
	private String errorDetails;

	public ErrorMessage() {
	}

	public ErrorMessage(String errorMessage, int errorCode, String errorDetails) {
		super();
		this.errorMessage = errorMessage;
		this.errorCode = errorCode;
		this.errorDetails = errorDetails;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorDetails() {
		return errorDetails;
	}

	public void setErrorDetails(String errorDetails) {
		this.errorDetails = errorDetails;
	}

	@Override
	public String toString() {
		return "ErrorMessage [errorMessage=" + errorMessage + ", errorCode=" + errorCode + ", errorDetails="
				+ errorDetails + "]";
	}

}
