package com.hsbc.currency.exchangeRates.domain.service;

import java.util.Map;

import com.hsbc.currency.exchangeRates.domain.model.entity.ConversionRates;

/**
 * <h1>ExchangeRatesService</h1>
 * 
 * @author Md Javed
 * @version 1.0
 * @since 2020-10-13
 */
public interface ExchangeRatesService {

	public ConversionRates getCurrentRates(String symbols);

	public Map<String, ConversionRates> getHistoricalData(String symbols);
}
