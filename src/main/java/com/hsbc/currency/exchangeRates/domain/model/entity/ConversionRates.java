package com.hsbc.currency.exchangeRates.domain.model.entity;

import java.io.Serializable;
import java.util.HashMap;

import io.swagger.annotations.ApiModelProperty;

/**
 * <h1>ConversionRates</h1>
 * 
 * @author Md Javed
 * @version 1.0
 * @since 2020-10-13
 */
public class ConversionRates implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(notes = "base for exchange rates", name = "base symbol", required = true, value = "base name")
	private String base;
	@ApiModelProperty(notes = "date of exchange rates", name = "date", required = true, value = "exchange Date")
	private String date;
	@ApiModelProperty(notes = "exchange rates", name = "rates symbol", required = true, value = "rates")
	private HashMap<String, String> rates;

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public HashMap<String, String> getRates() {
		return rates;
	}

	public void setRates(HashMap<String, String> rates) {
		this.rates = rates;
	}

	@Override
	public String toString() {
		return "ConversionRates [base=" + base + ", date=" + date + ", rates=" + rates + "]";
	}

}
