package com.hsbc.currency.exchangeRates.domain.service;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.hsbc.currency.exchangeRates.domain.model.entity.ConversionRates;
import com.hsbc.currency.exchangeRates.exception.DataNotFoundException;

/**
 * <h1>ExchangeRatesServiceImpl</h1>
 * 
 * @author Md Javed
 * @version 1.0
 * @since 2020-10-13
 */
@Service("exchangeRatesService")
public class ExchangeRatesServiceImpl implements ExchangeRatesService {
	private static final String SYMBOLS = "symbols";
	private static final String LOCATION_DISPLAY_FORMAT = "%s%s?%s=%s";
	@Value("${api.rates.api.baseurl}")
	private String baseUrl;

	/**
	 * This method will connect with third party exchange rates api.
	 * 
	 * @param symbolsParameter
	 *            used as query param to call exchange rates api.
	 * @return latest foreign exchange reference rates.
	 */
	@Override
	public ConversionRates getCurrentRates(final String symbolsParameter) {
		ResponseEntity<ConversionRates> response = null;
		try {
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<String> entity = buildHttpHeadersAndEntity();
			response = restTemplate.exchange(
					String.format(LOCATION_DISPLAY_FORMAT, baseUrl, "latest", SYMBOLS, symbolsParameter),
					HttpMethod.GET, entity, ConversionRates.class);
		} catch (Exception e) {
			throw new DataNotFoundException("data not found for the Symbol " + symbolsParameter);
		}
		return response.getBody();
	}

	/**
	 * This method will connect with third party exchange rates api.
	 * 
	 * @param symbolsParameter
	 *            used as query param to call exchange rates api.
	 * @return historical rates for the past six months for the same day.
	 */
	@Override
	public Map<String, ConversionRates> getHistoricalData(final String symbolsParameter) {
		ResponseEntity<ConversionRates> response;
		Map<String, ConversionRates> historicalData = new HashMap<>();
		try {

			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<String> entity = buildHttpHeadersAndEntity();
			Map<String, String> pastDatesOfSameDay = getDates();

			for (int i = 6; i > 0; i--) {
				String date = pastDatesOfSameDay.get("Month" + i);
				String uri = String.format(LOCATION_DISPLAY_FORMAT, baseUrl, date, SYMBOLS, symbolsParameter);
				response = restTemplate.exchange(uri, HttpMethod.GET, entity, ConversionRates.class);
				updateHistoricalData(response, historicalData, i);
			}

		} catch (Exception e) {
			throw new DataNotFoundException("data not found for the Symbol " + symbolsParameter);
		}
		return historicalData;
	}

	/**
	 * This method will update response for historicalData .
	 * 
	 * @param response
	 *            used to get Body if available.
	 * @return nothing.
	 */
	private void updateHistoricalData(ResponseEntity<ConversionRates> response,
			Map<String, ConversionRates> historicalData, int i) {
		if (response != null && response.getBody() != null) {
			historicalData.put("Month" + i, response.getBody());
		}
	}

	/**
	 * This method will create the HttPEntity which will use to call exchange
	 * rates api .
	 * 
	 * @param No
	 *            param.
	 * @return HttpEntity.
	 */
	private HttpEntity<String> buildHttpHeadersAndEntity() {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("user-agent",
				"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
		return new HttpEntity<String>("parameters", headers);
	}

	/**
	 * This method will create map of dates for the past six months for the same
	 * day.
	 * 
	 * @param No
	 *            param.
	 * @return past six month date.
	 */
	private Map<String, String> getDates() {
		Map<String, String> pastDatesOfSameDay = new HashMap<>();
		LocalDate currentDate = LocalDate.now();
		for (int i = 6; i > 0; i--) {
			pastDatesOfSameDay.put("Month" + i, currentDate.plusMonths(-i).toString());
		}
		return pastDatesOfSameDay;
	}

}
