package com.hsbc.currency.exchangeRates.resources;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hsbc.currency.exchangeRates.domain.model.entity.ConversionRates;
import com.hsbc.currency.exchangeRates.domain.service.ExchangeRatesService;
import com.hsbc.currency.exchangeRates.exception.DataNotFoundException;
import com.hsbc.currency.exchangeRates.exception.GenericException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * <h1>ExchangeRatesController</h1>
 * 
 * @author Md Javed
 * @version 1.0
 * @since 2020-10-13
 */
@Api(value = "Swagger2DemoRestController", description = "REST APIs related to ConversionRates Entity!!!!")
@RestController
@RequestMapping("/v1/exchange/rates")
public class ExchangeRatesController {

	protected static final Logger LOGGER = Logger.getLogger(ExchangeRatesController.class.getName());

	protected ExchangeRatesService exchangeRatesService;

	@Autowired
	public ExchangeRatesController(ExchangeRatesService exchangeRatesService) {
		this.exchangeRatesService = exchangeRatesService;
	}

	@ApiOperation(value = "Get current exchange rates ", response = Iterable.class, tags = "getCurrentRates")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success|OK"),
			@ApiResponse(code = 401, message = "not authorized!"), @ApiResponse(code = 403, message = "forbidden!!!"),
			@ApiResponse(code = 404, message = "data not found!!!") })
	@GetMapping(path = "/currentRates/{symbols}", produces = "application/json")
	public ResponseEntity<ConversionRates> getCurrentRates(@PathVariable("symbols") String symbols) {
		LOGGER.info(String.format("exchangeRatesService getCurrentRates() invoked:{} for {} ",
				exchangeRatesService.getClass().getName(), "current Rates"));
		ConversionRates conversionRates = null;
		try {
			conversionRates = exchangeRatesService.getCurrentRates(symbols);
		} catch (DataNotFoundException ex) {
			reThrowDataNotFoundException(ex);
		} catch (Exception ex) {
			reThrowGenericException(ex);
		}
		return new ResponseEntity<>(conversionRates, HttpStatus.OK);
	}

	@ApiOperation(value = "Get historical exchange rates  ", response = Iterable.class, tags = "getHistoricaltRates")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success|OK"),
			@ApiResponse(code = 401, message = "not authorized!"), @ApiResponse(code = 403, message = "forbidden!!!"),
			@ApiResponse(code = 404, message = "not found!!!") })
	@GetMapping(path = "/historicalData/{symbols}", produces = "application/json")
	public ResponseEntity<Map<String, ConversionRates>> getHistoricaltRates(@PathVariable("symbols") String symbols) {
		Map<String, ConversionRates> historicalRates = new HashMap<>();
		try {
			historicalRates = exchangeRatesService.getHistoricalData(symbols);
		} catch (DataNotFoundException ex) {
			reThrowDataNotFoundException(ex);
		} catch (Exception ex) {
			reThrowGenericException(ex);
		}
		return historicalRates != null ? new ResponseEntity<>(historicalRates, HttpStatus.OK)
				: new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	private void reThrowGenericException(final Exception ex) {
		LOGGER.log(Level.SEVERE, "Exception raised currentRates REST Call", ex);
		throw new GenericException(ex.getMessage());
	}

	private void reThrowDataNotFoundException(final DataNotFoundException ex) {
		LOGGER.log(Level.SEVERE, "Exception raised currentRates REST Call", ex);
		throw new DataNotFoundException(ex.getMessage());
	}

}
