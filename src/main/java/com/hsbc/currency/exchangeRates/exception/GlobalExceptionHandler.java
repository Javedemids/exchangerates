package com.hsbc.currency.exchangeRates.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import com.hsbc.currency.exchangeRates.domain.model.ErrorMessage;

@SuppressWarnings({ "unchecked", "rawtypes" })
@ControllerAdvice
public class GlobalExceptionHandler {

	// Handle specific exception

	@ExceptionHandler(DataNotFoundException.class)
	public ResponseEntity<?> HandleDataNotFoundException(DataNotFoundException exception, WebRequest request) {
		ErrorMessage errorMessage = new ErrorMessage(exception.getMessage(), 404, request.getDescription(false));
		return new ResponseEntity(errorMessage, HttpStatus.NOT_FOUND);
	}

	// Handle Application specific Generic exception
	@ExceptionHandler(GenericException.class)
	public ResponseEntity<?> HandleGenericException(GenericException exception, WebRequest request) {
		ErrorMessage errorMessage = new ErrorMessage(exception.getMessage(), 500, request.getDescription(false));
		return new ResponseEntity(errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	// Handle global exception
	@ExceptionHandler(Exception.class)
	public ResponseEntity<?> HandleGlobalException(Exception exception, WebRequest request) {
		ErrorMessage errorMessage = new ErrorMessage(exception.getMessage(), 500, request.getDescription(false));
		return new ResponseEntity(errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
