# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Skills Assessment – Java Developer
* 1.0

### How do I get set up? ###

* clone the repo
* import as a maven project
* update maven project
* Run as application


### Who do I talk to? ###

* MD Javed

### END Point details ###
* Swagger UI :   [Swegger UI](http://localhost:8080/swagger-ui.html#)
* currentRates:  [example for Current Rates](http://localhost:8080/v1/exchange/rates/currentRates/USD,GBP,HKD)
* historicalData :[example for Historical Rates](http://localhost:8080/v1/exchange/rates/historicalData/USD,GBP,HKD)